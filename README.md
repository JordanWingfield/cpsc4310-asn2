# README # 

To run the python code, simply have python installed and as a command enter:

(question 2)
python asn2_q2.py

and then, 

(question 5)
python asn2_q5.py


Both programs are closed systems, so they do not take in external input, but still run the functions required in the questions.

Question 5 uses two folders containing text files that are read as training data for the spam rater. To add more or less data, and still have the programs run, simply modify the text files in the files found in the enron3 folder.

The paths are enron3/ham/, and enron3/spam