#---------------------------------------
# CPSC4310 - Natural Language Processing
# Assignment 2 - Question 2
# Author: J. Wingfield 001175780
# University of Lethbridge
#---------------------------------------

import nltk
from nltk import word_tokenize
from nltk import FreqDist
from nltk.util import ngrams
from nltk.corpus import gutenberg
import random

# Corpus getter method:
# ---------------------------------------------------
# Input: string filename of corpus to use in gutenberg corpora
# Output: string representation of chosen corpus
def get_corpus(filename):
	words = nltk.corpus.gutenberg.words(filename)
	index = 0
	corpus = []
	while index < len(words):
		corpus.append(str(words[index]))
		index += 1
	clean_corpus = ""
	for word in corpus:
		clean_corpus += str(word) + " "
	return clean_corpus
	
# Senetence generator:
# ---------------------------------------------------
# Will create and output a sentence by using a given language model.
# The sentence will begin with some random unigram, and then continue following
# bigrams until a '.' is found.
# ---------------------------------------------------
# Input: l_model a language model made of some ngram
# Output: string sentence based off the given language model
def generate_sentence(ngram, fdist):
	# Generate the frequency and probability tables via given language
	# model:
	index = random.randint(0, len(fdist)-1)
	seed_word = fdist[index]
	while seed_word is '.' or seed_word is ',' or seed_word is '?' or seed_word is '!' or seed_word is '\'' or seed_word is "--" or seed_word is ';' or seed_word is':':
		index = random.randint(0, len(fdist)-1)
		seed_word = fdist[index]

	model_freq = nltk.ConditionalFreqDist(ngram)
	model_prob = nltk.ConditionalProbDist(model_freq, nltk.MLEProbDist)

	word = model_prob[seed_word].generate()

	chars = list(seed_word)
	chars[0] = chars[0].upper()
	sentence = ''.join(chars)
	sentence += " " + word

	# Cycle through words that are related to the given word at the end of the sentence based on the given language model:
	while word is not '!' and word is not '?':
		word = model_prob[word].generate()
		if word is '.':
			if (last_word("Mr", sentence) is True) or (last_word("Mrs", sentence) is True) or (last_word("Ms", sentence) is True):
				sentence += word
			else:
				sentence += word
				return sentence
				
		elif word is ',' or word is ';' or word is ':' or word is "'" or word is '!' or word is '?':
				sentence += word
		else:
			sentence += " " + word

	# Generate sentence
	return sentence
	
# Last word check method:
# Input: string to be matched, and base string to be matched against
# Output: true if string is part of the set of base_string at the end, and false
# if not.
def last_word(string, base_string):
	index = len(string)-1
	base_index = len(base_string)-1
	while index >= 0:
		if string[index] != base_string[base_index]:
			return False
		index -= 1
		base_index -= 1
	return True

# Main:	
corpus = get_corpus('austen-emma.txt')

tokens = nltk.word_tokenize(corpus)
fdist = FreqDist(tokens)

fdist_list = []
for word in fdist:
	index = 0
	while index < fdist[word]:
		fdist_list.append(word)
		index += 1

bigrams = ngrams (tokens, 2)

print (generate_sentence(bigrams, fdist_list))


