#--------------------------------------------------------
# CPSC4310 - Natural Language Processing
# Assignment 2 - Question 5
# Author: J. Wingfield 001175780
# University of Lethbridge
#--------------------------------------------------------

import nltk
from nltk import word_tokenize
from nltk import WordNetLemmatizer
from nltk import NaiveBayesClassifier
from nltk import classify
from nltk import MaxentClassifier
from nltk.corpus import stopwords
import random
import os
import glob
import re

# ----------------------------------------------
# Feature generation method:
# Input: string representation of email
# Output: dictionary of stemmed words
# ----------------------------------------------
# Stem words in a string, and tag them as commondwords, if there are any
# return final output
# ----------------------------------------------
def email_features(sent):
    features = {}
    wordtokens = [wordlemmatizer.lemmatize(word.lower())for word in word_tokenize(sent)]
    for word in wordtokens:
        if word not in commonwords:
            features[word] = True
    return features

# ----------------------------------------------
# Ascii character normalization method:
# Input: file to be read
# Output: string void of any non-ascii character text
# ----------------------------------------------
def ascii_normalize(text_file):
    text = text_file.read()
    normalized = (c for c in text if 0 < ord(c) < 127)
    return ''.join(normalized)

# Main:
wordlemmatizer = WordNetLemmatizer()
commonwords = stopwords.words('english')
ham_text = []
spam_text = []

# Retrieve sample ham email text, and add to list
print ("Appending ham text . . . ")
for infile in glob.glob(os.path.join('enron3/ham/', '*.txt')):
    text_file = open (infile, "r")
    text = ascii_normalize(text_file)
    ham_text.append(text)
    text_file.close()

# Retrieve sample spam email text, and add to list
print ("Finished ham.")
print ("Appending spam text. . .")
for infile in glob.glob(os.path.join('enron3/spam/', '*.txt')):
    text_file = open (infile, "r")
    text = ascii_normalize(text_file)
    spam_text.append(text)
    text_file.close()

# Combine total samples of spam and ham messages into one list, and shuffle
print ("Finished spam.")
mixedemails = ([(email, 'spam') for email in spam_text])
mixedemails += ([(email, 'ham') for email in ham_text])

random.shuffle(mixedemails)

# Classify features for classifier to use
print ("Creating feature set. . . (This may take a few minutes due to large file sets)")
featureset = [(email_features(n), g) for (n, g) in mixedemails]
print ("Finished creating feature set.")

# Training and testing
print ("Training and testing. . .")
print (" ")
num_folds = 10
size = int(len(featureset) / num_folds)

total_precision = 0.0
total_recall = 0.0

# Go through given number of folds for training on majority, and testing on the rest.
for i in range(num_folds):
    index_test_begin = i * size
    index_test_end = index_test_begin + size

    true_positives = 0
    true_negatives = 0
    false_positives = 0
    false_negatives = 0

    # Split total feature set based on where test_set currently is
    train_set = featureset[:index_test_begin]+featureset[index_test_end:]
    test_set = featureset[index_test_begin:index_test_end]
    classifier = NaiveBayesClassifier.train(train_set)

    # Test and record results of classifications
    for (email, real_classification) in test_set:
        guess_classification = classifier.classify(email)
        if guess_classification is "ham":
            if guess_classification is real_classification:
                true_positives += 1
            else:
                false_positives += 1
        elif guess_classification is "spam":
            if guess_classification is real_classification:
                true_negatives += 1
            else:
                false_negatives += 1

    precision = float(true_positives) / float(true_positives + false_positives)
    recall = float(true_positives) / float(true_positives + false_negatives)

    total_precision += precision
    total_recall += recall

    # Report minor results for current fold
    print ("Round {} Accuracy: {}").format(i+1, classify.accuracy(classifier, test_set))
    print ("Round {} Precision: {}").format(i+1, precision)
    print ("Round {} Recall: {}").format(i+1, recall)
    print (" ")

# Report major results for entire training and testing
total_precision = total_precision / num_folds
total_recall = total_recall / num_folds

print ("-------------------------------------")
print ("Results:")
print ("-------------------------------------")
print ("Total Precision: {}").format(total_precision)
print ("Total Recall: {}").format(total_recall)
