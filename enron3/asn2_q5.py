#--------------------------------------------------------
# CPSC4310 - Natural Language Processing
# Assignment 2 - Question 5
# Author: J. Wingfield 001175780
# University of Lethbridge
#--------------------------------------------------------

import nltk
from nltk import word_tokenize
from nltk import WordNetLemmatizer
from nltk import NaiveBayesClassifier
from nltk import classify
from nltk import MaxentClassifier
from nltk.corpus import stopwords
import random
import os
import glob
import re

def email_features(sent):
    features = {}
    wordtokens = [wordlemmatizer.lemmatize(word.lower())for word in word_tokenize(sent)]
    for word in wordtokens:
        if word not in commonwords:
            features[word] = True
    return features

def ascii_normalize(text_file):
    text = text_file.read()
    normalized = (c for c in text if 0 < ord(c) < 127)
    return ''.join(normalized)

# Main:
wordlemmatizer = WordNetLemmatizer()
commonwords = stopwords.words('english')
ham_text = []
spam_text = []

print ("Appending ham text . . . ")
for infile in glob.glob(os.path.join('ham/', '*.txt')):
    text_file = open (infile, "r")
    text = ascii_normalize(text_file)
    ham_text.append(text)
    text_file.close()

print ("Finished ham.")
print ("Appending spam text. . .")
for infile in glob.glob(os.path.join('spam/', '*.txt')):
    text_file = open (infile, "r")
    text = ascii_normalize(text_file)
    spam_text.append(text)
    text_file.close()

print ("Finished spam.")
mixedemails = ([(email, 'spam') for email in spam_text])
mixedemails += ([(email, 'ham') for email in ham_text])

random.shuffle(mixedemails)

print ("Creating feature set. . .")
featureset = [(email_features(n), g) for (n, g) in mixedemails]

size = int(len(featureset) * 0.7)

train_set = featureset[size:]
test_set = featureset[:size]

print ("Training. . .")
classifier = NaiveBayesClassifier.train(train_set)
print ("Finished training.")

print ("Labels: ", classifier.labels())
print ("Accuracy: ", classify.accuracy(classifier, test_set))

print ("Ready to evaluate. (CNTR-Z to Quit)")
while True:
    featset = email_features(raw_input("Enter text to classify: "))
    print classifier.classify(featset)
